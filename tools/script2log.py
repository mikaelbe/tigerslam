#!/usr/bin/python
import os.path
import sys
import re
import datetime
from optparse import OptionParser  # <= 2.6. Use argparse for >= 2.7

class LidarReading(object):
    
    def __init__(self, timestamp, range, measurements):
        self.timestamp = timestamp
        self.range = range
        self.measurements = measurements
    
    def logEntry(self):
        s = "LIDAR"
        s += self.timestamp.strftime(",%m-%d-%Y.%H:%M:%S.%f000 WEST")
        s += "," + self.range
        for m in self.measurements:
            if m >= 8183:
                s += ",0"
            else:
                s += "," + "%d0" % (m,)
        s += "\n"
        return s

def read_laser_ranges(infile, outfile):
    of = open(outfile, 'w')
    pattern = re.compile(r"^LASER-RANGE\s+(\d+)\s+(\d+)\s+0\s+\d+\s+(\d+\.\d):\s(.+)")
    
    readings = []
    for line in open(infile):
        match = pattern.match(line)
        if match:
            
            # Get the timestamp
            timestamp = datetime.datetime.fromtimestamp(float(match.group(1) + "." + match.group(2)))
            
            # Write the measurements
            measurements = []
            for measurement in match.group(4).split():
                measurements.append(float(measurement))

            readings.append(LidarReading(timestamp, match.group(3), measurements))
    
    # Sort by timestamp
    readings.sort(key=lambda reading: reading.timestamp)
    
    for reading in readings:
        of.write(reading.logEntry())
    
    

def main(args=None):
    # Allows calling main directly with args passed as a list.
    if args is None:
        args = sys.argv

    parser = OptionParser()
    parser.add_option("-i", "--infile", dest="infile",
                      help="input file", metavar="FILE")
    parser.add_option("-o", "--outfile", dest="outfile",
                      help="output file", metavar="FILE")
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print status messages to stdout")

    (options, args) = parser.parse_args(args)
    
    read_laser_ranges(options.infile, options.outfile)
    
    return 0

if __name__ == "__main__":
    sys.exit(main())