function [  ] = plotpath( filename, map )
%PLOTPATH Summary of this function goes here
%   Detailed explanation goes here

figure;

img = imread(map);
axis([0 102.4 0 102.4]);
imshow(img);
hold on
path = csvread(filename);

plot(path(:, 1).*10, path(:, 2).*10, '-o')

hold off;

end

