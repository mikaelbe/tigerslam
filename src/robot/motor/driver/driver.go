// Package driver does communication to the robot motor driver card.
//
// It communicates over a serial interface.
package driver

import (
	"errors"
	"fmt"
	"io"
	"log"

	serial "github.com/tarm/goserial"

	"robot/config"
	"robot/logging"
	"robot/tools/intmath"
)

type Direction int
type Side int

var logger *log.Logger

const (
	RANGE = 2000
)

const (
	DIRECTION_FORWARD = iota
	DIRECTION_BACKWARD
)

const (
	SIDE_LEFT = iota
	SIDE_RIGHT
)

// The motor struct holds parameters for communicating with the motor driver
// card and the driver card's parameters, and implements methods for
// commuication.
type Motor struct {
	config      *serial.Config
	port        io.ReadWriteCloser
	speed_left  int
	speed_right int
}

func init() {
	logger = logging.New()
}

// Make an arbitrary motor.
func MakeMotor(config *serial.Config) *Motor {
	return &Motor{
		config: config,
	}
}

// Make default motor from standard settings and config file.
func MakeDefaultMotor() *Motor {
	return &Motor{
		config: &serial.Config{
			Name: config.MOTORS_COM_NAME,
			Baud: config.MOTORS_BAUD_RATE,
		},
	}
}

// Set up the connection to the motor driver card over serial interface.
func (m *Motor) Connect() error {
	s, err := serial.OpenPort(m.config)
	if err != nil {
		return err
	}

	logger.Println("Motor connected.")
	m.port = s
	return nil
}

// Disconnect
func (m *Motor) Disconnect() error {
	if m.port == nil {
		return nil
	}

	err := m.port.Close()
	if err != nil {
		return err
	}

	logger.Println("Motor disconnected.")
	m.port = nil
	return nil
}

func (m *Motor) IsConnected() bool {
	return m.port != nil
}

// Set the speeds of the left and right motors, using floating point numbers
// between -1 and 1, indicating speeds within the range of the motors. 1
// indicates the maximum speed, while 0 is stopped and -1 is maximum backward
// speed.
func (m *Motor) SetSpeeds(leftDecimal, rightDecimal float64) error {

	if !m.IsConnected() {
		err := m.Connect()
		if err != nil {
			return err
		}
	}

	// Incoming are [-1, 1]{float}. Convert to [-RANGE, RANGE]{int}
	leftInt := int(leftDecimal * float64(RANGE))
	rightInt := int(rightDecimal * float64(RANGE))

	// Check if we're inside limits
	// if intmath.Abs(leftInt) > RANGE || intmath.Abs(rightInt) > RANGE {
	// 	return errors.New("Outside range")
	// }

	// Cut to within range
	leftInt = intmath.Max(intmath.Min(leftInt, RANGE), -RANGE)
	rightInt = intmath.Max(intmath.Min(rightInt, RANGE), -RANGE)

	var err error

	// Sets the left side
	err = m.setSideSpeed(SIDE_LEFT, leftInt)
	if err != nil {
		m.Disconnect()
		return err
	}

	// Sets the right side
	err = m.setSideSpeed(SIDE_RIGHT, rightInt)
	if err != nil {
		m.Disconnect()
		return err
	}

	return nil
}

// Set speed of one motor with implicit direction (sign of speed)
func (m *Motor) setSideSpeed(side Side, speed int) error {

	var direction Direction
	if speed >= 0 {
		direction = DIRECTION_FORWARD
	} else {
		direction = DIRECTION_BACKWARD
	}

	return m.setSpeedWithDirection(side, direction, intmath.Abs(speed))
}

// Set the speed of one motor with direction as a parameter
func (m *Motor) setSpeedWithDirection(side Side, direction Direction, speed int) error {

	if !m.IsConnected() {
		return errors.New("Motor not connected.")
	}

	// Check if speed is allowable
	if intmath.Abs(speed) > RANGE {
		return errors.New("Speed outside allowable range")
	}

	var num int
	if direction == DIRECTION_FORWARD {
		num = speed
	} else {
		num = -speed
	}

	speed = speed / 2

	// Communicate with motor card
	com := fmt.Sprintf("%s%s.%s", side, direction, speedFormat(speed))
	err := m.write(com)
	if err != nil {
		return err
	}

	// Write the change to motor
	if side == SIDE_LEFT {
		m.speed_left = num
	} else {
		m.speed_right = num
	}

	return nil
}

// Write string to the serial connection
func (m *Motor) write(s string) error {
	_, err := m.port.Write([]byte(s))
	return err
}

// Read string from the serial connection
func (m *Motor) read() (string, error) {
	buf := make([]byte, 128)
	n, err := m.port.Read(buf)
	return string(buf[:n]), err
}

func (s Side) String() string {
	if s == SIDE_LEFT {
		return "v"
	}
	return "h"
}

func (d Direction) String() string {
	if d == DIRECTION_FORWARD {
		return "+"
	}
	return "-"
}

func speedFormat(speed int) string {
	return fmt.Sprintf("%.4d", speed)
}
