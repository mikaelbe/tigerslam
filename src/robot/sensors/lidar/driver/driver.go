// Package driver implements basic communication with a URG-04LX-UG01 device
// from HOKUYO.
//
// The code relies on the C driver files provided by HOKUYO, more specifically
// the urg-0.8.18 zip file, which provides source files and a number of sample
// applications.
//
// Using the HOKUYO source files on Linux is pretty straight forward. However,
// a bit of extra work was demanded to get this to work with Cgo on Windows.
// Building the library can be done with MinGW. Download the MSYS executable
// installer and proceed to install it, but do not install the compilers. Also
// download a release of the MinGW-w64 toolchain, e.g. "rubenvb"'s personal
// build. Unzip this build into the MinGW folder, as C:\MinGW\mingw64. Now,
// open the MinGW Shell (from start menu), and mount the mingw64-folder with
//
// 		mount 'C:\MinGW\mingw64\' /mingw64
//
// Then, navigate to the urg-0.8.18 folder, and configure with
//
// 		./configure --prefix=/mingw64 LDFLAGS=-lwinmm
//
// before running "make" and "make install". Now, a series of .a-files should
// be in the mingw64/lib folder. These should be linked against using the 
// LDFLAGS below. The project should now be ready to be built.
package driver

// #cgo CFLAGS: -I/MinGW/mingw64/include/c_urg
// #cgo LDFLAGS: /MinGW/mingw64/lib/libc_urg.a /MinGW/mingw64/lib/libc_urg_connection.a /MinGW/mingw64/lib/libc_urg_system.a -lm -lwinmm
// #include "urg_ctrl.h"
// #include <stdio.h>
// #include <stdlib.h>
//
// long atLong(long* array, int index) {
// 		return array[index];
// }
import "C"

import (
	"errors"
	"fmt"
	"unsafe"

	"robot/config"
)

// Request types for the URG
type RequestType int32

const (
	URG_GD = iota
	URG_GD_INTENSITY
	URG_GS
	URG_MD
	URG_MD_INTENSITY
	URG_MS
)

const (
	INFINITY_TIMES = C.UrgInfinityTimes
)

// Urg holds the connection information and the C urg structure, and its methods
// implement the communication with an URG device.
type Urg struct {
	// The device port where the URG device is, e.g. "COM7"
	device string
	// The baud rate of the port, e.g. 115200
	baudRate int
	// The minimum step to measure. See manual for more explaination
	minStep int
	// The maximum measured step
	maxStep int
	// The minimum measureable distance
	minDistance int
	// The maximum measureable distance
	maxDistance int

	dataMax int
	curg    *C.urg_t
	cdata   *C.long
}

// Make URG from device string and baud rate
func MakeUrg(device string, baudRate, minStep, maxStep int) *Urg {
	u := &Urg{
		device:   device,
		baudRate: baudRate,
		minStep:  minStep,
		maxStep:  maxStep,
		curg:     new(C.urg_t),
	}

	return u
}

// Make the default URG from config
func MakeDefaultUrg() *Urg {
	return MakeUrg(config.LIDAR_COM_NAME, config.LIDAR_BAUD_RATE, 44, 725)
}

// Connect to the URG
func (u *Urg) Connect() error {
	var err error

	r := C.urg_connect(u.curg, C.CString(u.device), C.long(u.baudRate))
	if r < 0 {
		return errors.New(fmt.Sprintf("Unable to connect to device %s at baudrate %d", u.device, u.baudRate))
	}

	// Find minimum measureable distance
	u.minDistance, err = u.MinDistance()
	if err != nil {
		return err
	}

	// Find maximum measureable distance
	u.maxDistance, err = u.MaxDistance()
	if err != nil {
		return err
	}

	// Find DataMax
	u.dataMax, err = u.DataMax()
	if err != nil {
		return err
	}

	// Set up cdata, the array the c writes distance data to
	var dummy C.long
	size := int(unsafe.Sizeof(dummy))
	u.cdata = (*C.long)(C.malloc(C.size_t(size * u.dataMax)))

	return nil
}

// Disconnect from the URG
func (u *Urg) Disconnect() {
	C.urg_disconnect(u.curg)
}

// Check if an URG is connected
func (u *Urg) IsConnected() bool {
	return int(C.urg_isConnected(u.curg)) == 1
}

// Get the URG Model Type as a string
func (u *Urg) GetModel() string {
	return C.GoString(C.urg_model(u.curg))
}

// Get Error
func (u *Urg) Error() error {
	return errors.New(C.GoString(C.urg_error(u.curg)))
}

// Set Skip Lines, a.k.a. Clustering
//
// The volume of aquired data can be reduced by skipping lines. This is the same
// as "clustering" in the URG product manual. Skipping lines will cause the URG
// to return the minimum value over the cluster of values, thus reducing the
// number of data points returned by a factor.
func (u *Urg) SetSkipLines(lines int) error {
	if int(C.urg_setSkipLines(u.curg, C.int(lines))) < 0 {
		return u.Error()
	}
	return nil
}

// Get data max
func (u *Urg) DataMax() (int, error) {
	dataMax := int(C.urg_dataMax(u.curg))

	if dataMax < 0 {
		return dataMax, u.Error()
	}

	return dataMax, nil
}

// Get minimum measureable distance
func (u *Urg) MinDistance() (int, error) {
	minDistance := int(C.urg_minDistance(u.curg))

	if minDistance < 0 {
		return minDistance, u.Error()
	}

	return minDistance, nil
}

// Get maximum measureable distance
func (u *Urg) MaxDistance() (int, error) {
	maxDistance := int(C.urg_maxDistance(u.curg))

	if maxDistance < 0 {
		return maxDistance, u.Error()
	}

	return maxDistance, nil
}

// Aquire the latest timestamp
func (u *Urg) RecentTimestamp() (int, error) {
	timestamp := int(C.urg_recentTimestamp(u.curg))

	if timestamp < 0 {
		return timestamp, u.Error()
	}

	return timestamp, nil
}

// Request Infinite Data
//
// Use the URG MD word and request INFINITY_TIMES data. Subsequent calls to
// ReceiveData() will return fresh data.
func (u *Urg) RequestInfiniteData() error {
	// Set capture times
	if C.urg_setCaptureTimes(u.curg, INFINITY_TIMES) < 0 {
		return u.Error()
	}

	// Request data
	if C.urg_requestData(u.curg, URG_MD, C.int(u.minStep), C.int(u.maxStep)) < 0 {
		return u.Error()
	}

	return nil
}

// Read MD data
func (u *Urg) ReceiveData() ([]float64, error) {
	data := make([]float64, u.maxStep-u.minStep+1)

	ret := int(C.urg_receiveData(u.curg, u.cdata, C.int(u.dataMax)))
	if ret < 0 {
		return nil, u.Error()
	}

	var measurement float64
	for i := u.minStep; i <= u.maxStep; i++ {
		measurement = float64(C.atLong(u.cdata, C.int(i)))

		// Use only valid measurements
		//		if measurement > u.minDistance && measurement < u.maxDistance {
		data[i-u.minStep] = measurement
		//		}
	}

	return data, nil
}
